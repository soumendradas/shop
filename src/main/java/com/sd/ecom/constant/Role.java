package com.sd.ecom.constant;


public enum Role {
	
	ADMIN, BILLER, ACCOUNTANT
}
