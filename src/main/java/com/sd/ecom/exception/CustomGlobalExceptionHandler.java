package com.sd.ecom.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import com.sd.ecom.payload.ErrorMessage;

@RestControllerAdvice
public class CustomGlobalExceptionHandler {
	
	@ExceptionHandler(ShopNotFoundException.class)
	public ResponseEntity<ErrorMessage> handleShopNotFoundException(ShopNotFoundException e){
		
		return ResponseEntity.internalServerError().body(new ErrorMessage(
				new Date().toString(),
				e.getMessage(), 
				HttpStatus.INTERNAL_SERVER_ERROR.value(), 
				HttpStatus.INTERNAL_SERVER_ERROR.toString()));
	}
	
	@ExceptionHandler(NoResourceFoundException.class)
	public ResponseEntity<ErrorMessage> handleNoResourceFoundException(NoResourceFoundException e){
		
		return new ResponseEntity<ErrorMessage>(new ErrorMessage(
				new Date().toString(),
				e.getMessage(),
				e.getStatusCode().value(),
				e.getStatusCode().toString()
				),
				e.getStatusCode());
	}
	
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public ResponseEntity<ErrorMessage> handleHttpRequestMethodNotSupportedException(
			HttpRequestMethodNotSupportedException e){
		
		return new ResponseEntity<ErrorMessage>(new ErrorMessage(
				new Date().toString(),
				e.getMessage(),
				e.getStatusCode().value(),
				e.getStatusCode().toString()),
				e.getStatusCode());
	}
}
