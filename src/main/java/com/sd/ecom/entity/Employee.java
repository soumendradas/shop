package com.sd.ecom.entity;

import org.hibernate.type.TrueFalseConverter;

import com.sd.ecom.constant.Role;
import com.sd.ecom.embeded.BankAccount;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Entity
@Data
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long empId;
	
	private String empName;
	
	private String empEmail;
	
	private String empMob;
	
	@Enumerated(EnumType.STRING)
	private Role empRole;
	
	@Embedded
	private BankAccount empBankAccount;
	
	@Column(unique = true)
	private String empUserID;
	
	private String empPassword;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "emp_shop_id")
	private Shop empShop;
	
	@ManyToOne
	@JoinColumn(name = "emp_current_address_id")
	private Address currentAddress;
	
	@ManyToOne
	@JoinColumn(name = "emp_permanet_address_id")
	private Address permanetAddress;
}
