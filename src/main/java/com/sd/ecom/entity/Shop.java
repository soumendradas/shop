package com.sd.ecom.entity;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sd.ecom.embeded.BankAccount;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import lombok.Data;

@Entity
@Data
public class Shop {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long shopId;
	
	private String shopName;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "shop_address_id")
	private Address shopAddress;
	
	@JsonIgnore
	@OneToMany(mappedBy = "empShop",
			fetch = FetchType.LAZY,
			targetEntity = Employee.class)
	private Set<Employee> employees;
	
	@Embedded
	private BankAccount shopBankAc;
}
