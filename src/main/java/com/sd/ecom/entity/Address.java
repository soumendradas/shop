package com.sd.ecom.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Address {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long addressId;
	
	private Integer pin;
	
	private String country;
	
	private String state;
	
	private String area;
	
	private String flatNo;
	private String city;
	
	private String postOffice;
	
	private String policeStation;
	
	

}
