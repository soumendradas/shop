package com.sd.ecom.embeded;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Data;

@Embeddable
@Data
public class BankAccount {
	
	@Column(name = "bank_account_no", unique = true)
	private String accountNo;
	
	@Column(name = "bank_ifsc_code")
	private String ifscCode;
	
	@Column(name = "bank_name")
	private String bankName;
	

}
