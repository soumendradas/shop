package com.sd.ecom.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage {
	
	private String timeStamp;
	
	private String message;
	
	private Integer status;
	
	private String code;

}
