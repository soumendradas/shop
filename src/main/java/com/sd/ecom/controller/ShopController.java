package com.sd.ecom.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sd.ecom.entity.Employee;
import com.sd.ecom.entity.Shop;
import com.sd.ecom.exception.ShopNotFoundException;
import com.sd.ecom.service.ShopService;

@RestController
@RequestMapping("/shop")
public class ShopController {

	@Autowired
	private ShopService service;

	@PostMapping("/add")
	public ResponseEntity<String> addShop(@RequestBody Shop shop) {
		try {
			Long id = service.addShop(shop);
			String message = "Shop created with id: " + id;

			return new ResponseEntity<String>(message, HttpStatus.CREATED);
		} catch (ShopNotFoundException e) {
			// TODO: handle exception
			throw e;
		}
	}

	@GetMapping(value = { "", "/" })
	public ResponseEntity<List<Shop>> getAllShop() {
		List<Shop> shops = service.getAllShop();
		return ResponseEntity.ok(shops);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Shop> getShop(@PathVariable Long id) {
		try {
			Shop shop = service.getOneShop(id);
			return ResponseEntity.ok(shop);
		} catch (ShopNotFoundException e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	@PutMapping("/update")
	public ResponseEntity<String> updateShop(@RequestBody Shop shop){
		try {
			service.updateShop(shop);
			return new ResponseEntity<String>(
					"Shop updated successfully.", HttpStatus.ACCEPTED);
		}catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> deleteShop(@PathVariable Long id){
		try {
			service.deleteShop(id);
			return new ResponseEntity<String>(
					"Shop with id "+id+" deleted successfully",HttpStatus.ACCEPTED);
		}catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	@GetMapping("/emps/{shopId}")
	public ResponseEntity<Set<Employee>> showAllEmployees(@PathVariable Long shopId){
		
		try {
			Set<Employee> employees = service.getEmployess(shopId);
			return ResponseEntity.ok(employees);
		}catch (Exception e) {
			throw e;
		}
	}

}
