package com.sd.ecom.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sd.ecom.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
