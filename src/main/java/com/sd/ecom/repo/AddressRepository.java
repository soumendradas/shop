package com.sd.ecom.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sd.ecom.entity.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {

}
