package com.sd.ecom.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sd.ecom.entity.Shop;

public interface ShopRepository extends JpaRepository<Shop, Long> {

}
