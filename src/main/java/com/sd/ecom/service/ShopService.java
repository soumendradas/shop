package com.sd.ecom.service;

import java.util.List;
import java.util.Set;

import com.sd.ecom.entity.Employee;
import com.sd.ecom.entity.Shop;

public interface ShopService {
	
	Long addShop(Shop shop);
	
	List<Shop> getAllShop();
	
	Shop getOneShop(Long shopId);
	
	void updateShop(Shop shop);
	
	void deleteShop(Long shopId);
	
	Set<Employee> getEmployess(Long shopId);

}
