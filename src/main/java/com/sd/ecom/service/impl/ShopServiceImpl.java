package com.sd.ecom.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sd.ecom.entity.Employee;
import com.sd.ecom.entity.Shop;
import com.sd.ecom.exception.ShopNotFoundException;
import com.sd.ecom.repo.ShopRepository;
import com.sd.ecom.service.ShopService;

@Service
public class ShopServiceImpl implements ShopService {
	
	@Autowired
	private ShopRepository repo;

	@Override
	public Long addShop(Shop shop) {
		// TODO Auto-generated method stub
		try {
			Long id = repo.save(shop).getShopId();
			return id;
		}catch (Exception e) {
			// TODO: handle exception
			throw new ShopNotFoundException(e.getMessage());
		}
		
	}

	@Override
	public Shop getOneShop(Long shopId) {
		// TODO Auto-generated method stub
		Shop shop = repo.findById(shopId)
				.orElseThrow(()->new ShopNotFoundException("Id is invalid. Shop not found"));
		return shop;
	}

	@Override
	public void updateShop(Shop shop) {
		// TODO Auto-generated method stub
		
		if(repo.existsById(shop.getShopId())) {
			addShop(shop);
		}else {
			throw new ShopNotFoundException(
					"Shop not available in database with id: "+shop.getShopId());
		}

	}

	@Override
	public void deleteShop(Long shopId) {
		// TODO Auto-generated method stub
		repo.delete(getOneShop(shopId));

	}

	@Override
	public Set<Employee> getEmployess(Long shopId) {
		// TODO Auto-generated method stub
		
		return getOneShop(shopId).getEmployees();
	}

	@Override
	public List<Shop> getAllShop() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

}
